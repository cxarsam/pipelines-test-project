import React from 'react'

const HelloWorld = ({ name = 'Arsam' }) => <p data-testid="hello-message">Hello {name}!</p>

export default HelloWorld
