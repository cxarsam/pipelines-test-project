import React from 'react'
import { render } from '@testing-library/react'

import HelloWorld from './HelloWorld'

describe('<HelloWorld />', () => {
  let props

  beforeEach(() => {
    props = {
      name: 'World',
    }
  })

  const renderComponent = () => render(<HelloWorld {...props} />)

  it('Should say hello to the World!', () => {
    const { getByTestId } = renderComponent()

    const HelloWorld = getByTestId('hello-message')

    expect(HelloWorld).toHaveTextContent('Hello World!')
  })

  it('Should say hello to Arsam', () => {
    delete props.name
    const { getByTestId } = renderComponent()

    const HelloWorld = getByTestId('hello-message')

    expect(HelloWorld).toHaveTextContent('Hello Arsam!')
  })

  it('Should fail', () => {
    const { getByTestId } = renderComponent()

    const HelloWorld = getByTestId('hello-message')

    expect(HelloWorld).toHaveTextContent('Hello Arsam!')
  })
})
